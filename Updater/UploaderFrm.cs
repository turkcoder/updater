﻿using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Updater.Detmach.Models;
using Updater.Detmach.Servis;

namespace Updater.Detmach
{
    public partial class UploaderFrm : Form
    {
        #region Tanımlamalar
        /// <summary>
        /// Proje Adı
        /// </summary>
        private string ProjeAdi { get; set; }

        /// <summary>
        /// Projenin Versiyon Numarası
        /// </summary>
        private string ProjeVersiyonu { get; set; }

        /// <summary>
        /// Download Adresi (Sonuna Slash Katmayı Unutma - http://www.golyok.com/update/
        /// </summary>
        private string ProjeUrl { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Adresi ftp://golyok.com//httpdocs/wwwroot/upload/ (Eğer Bir Dizin Belirtecekseniz Lütfen Önce Dizini Ftp'de Oluşturunuz)
        /// </summary>
        private string FtpAdresi { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Kullanıcı Adı
        /// </summary>
        private string FtpKullaniciAdi { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Kullanıcı Şifresi
        /// </summary>
        private string FtpSifre { get; set; }
        #endregion
        public UploaderFrm(string _ProjeVersiyonu, string _ProjeAdi, string _FtpAdresi, string _FtpKullaniciAdi, string _FtpSifre, string _ProjeDownloadUrl)
        {
            InitializeComponent();
            ProjeVersiyonu = _ProjeVersiyonu;
            ProjeAdi = _ProjeAdi;
            FtpAdresi = _FtpAdresi;
            FtpKullaniciAdi = _FtpKullaniciAdi;
            FtpSifre = _FtpSifre;
            ProjeUrl = _ProjeDownloadUrl;
            DosyaAdi = ProjeAdi.kelimeDuzeltEng() + ".zip";
            WebDosyaListesiJson = ProjeAdi.kelimeDuzeltEng() + ".json";
            GuncellemeJson = ProjeAdi.kelimeDuzeltEng() + "-ver.xml";
            GuncellemeDosya = ProjeUrl + DosyaAdi;
        }
        private static readonly string ApplicationYol = Environment.CurrentDirectory;
        private readonly int folderOffset = ApplicationYol.Length + (ApplicationYol.EndsWith("\\") ? 0 : 1);
        private List<DosyaKontrolView> Dosyalar = new List<DosyaKontrolView>();
        private readonly string DosyaAdi;
        private readonly string WebDosyaListesiJson;
        private readonly string GuncellemeJson;
        private readonly string GuncellemeDosya;
        ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploaderFrm));
        private async void UploaderFrm_Load(object sender, EventArgs e)
        {
            //string.Format(resources.GetString("labelDescription.Text", CultureInfo.CurrentCulture));
 
             await GuncellemeYukle();

        }

        public async Task SetProggessBar(int a, int b)
        {
            pb.Maximum = b;
            pb.Value = a;
            await Task.CompletedTask;
        }
        public async Task SetMesaj(string mesaj)
        {
            lblMesaj.Text = mesaj;
            await Task.CompletedTask;
        }
        public async Task SetBaslik(string baslik)
        {
            lblBaslik.Text = baslik;
            await Task.CompletedTask;
        }
        public async Task GuncellemeYukle()
        {
            try
            {
                DosyaSil();
                #region Dosya Listeleri

                string WebFileList = await DosyaIndir(ProjeUrl + WebDosyaListesiJson);
                List<DosyaKontrolView> WebDosyaListesi;
                if (WebFileList == "Hata")
                {
                    WebDosyaListesi = new List<DosyaKontrolView>();
                }
                else
                {
                    WebDosyaListesi = JsonConvert.DeserializeObject<List<DosyaKontrolView>>(WebFileList);
                }
                List<string> DosyaListesi = TumDosyalar(ApplicationYol);
                foreach (var item in DosyaListesi)
                {
                    var DosyaZaman = File.GetLastWriteTime(item);
                    var dosya = WebDosyaListesi.Where(t => t.DosyaAdi == item.Substring(folderOffset)).FirstOrDefault();
                    if (dosya != null)
                    {
                        if (dosya.DegistirilmeTarihi != DosyaZaman)
                        {
                            WebDosyaListesi.Remove(dosya);
                            Dosyalar.Add(new DosyaKontrolView(item, DosyaZaman));
                        }
                    }
                    else { Dosyalar.Add(new DosyaKontrolView(item, DosyaZaman)); }
                }

                #endregion Dosya Listeleri

                #region Zipi Oluştur


                FileStream fsOut = File.Create(DosyaAdi);
                ZipOutputStream zipStream = new ZipOutputStream(fsOut);
                zipStream.SetLevel(9); //0-9, 9 being the highest level of compression

                SetMesaj("Dosyalar Sıkıştırılıyor");
                int s = 0;

                foreach (var t in Dosyalar)
                {
                    SetMesaj($"Dosyalar Sıkıştırılıyor {s}/{Dosyalar.Count}" );
                    s++;
                    string entryName = t.DosyaAdi.Substring(folderOffset);
                    WebDosyaListesi.Add(new DosyaKontrolView(entryName, t.DegistirilmeTarihi));
                    FileInfo fi = new FileInfo(t.DosyaAdi);
                    entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                    ZipEntry newEntry = new ZipEntry(entryName);
                    newEntry.Flags = (int)ICSharpCode.SharpZipLib.Zip.GeneralBitFlags.UnicodeText;
                    newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity
                    newEntry.Size = fi.Length;
                    zipStream.PutNextEntry(newEntry);

                    // Zip the file in buffered chunks
                    // the "using" will close the stream even if an exception occurs
                    byte[] buffer = new byte[4096];
                    using (FileStream streamReader = File.OpenRead(t.DosyaAdi))
                    {
                        StreamUtils.Copy(streamReader, zipStream, buffer);
                    }
                    zipStream.CloseEntry();
                    SetProggessBar(s, Dosyalar.Count());
                }
                zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
                zipStream.Close();
                #endregion Zipi Oluştur

                #region Dosya Listelerini Webe Yükle
                StreamWriter rd = new StreamWriter(WebDosyaListesiJson, false, Encoding.UTF8);

                rd.Write(JsonConvert.SerializeObject(WebDosyaListesi));
                rd.Close();
                await Upload(WebDosyaListesiJson, false);

                #endregion Dosya Listelerini Webe Yükle
                await GuncellemeDosyasiHazirla();
                
                await Upload(DosyaAdi, true);
                DosyaSil();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            this.Close();
        }
        /// <summary>
        /// Güncellemeleri Hazırlar
        /// </summary>
        /// <returns></returns>
        private async Task GuncellemeDosyasiHazirla()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("    ");
            settings.CloseOutput = true;
            settings.OmitXmlDeclaration = true;
            using (XmlWriter writer = XmlWriter.Create(GuncellemeJson, settings))
            {
                writer.WriteStartElement("item");
                writer.WriteElementString("version", ProjeVersiyonu);
                writer.WriteElementString("UploadTarihi", DateTime.Now.ToString());
                writer.WriteElementString("url", GuncellemeDosya);
                writer.WriteElementString("html", string.Empty);
                writer.WriteElementString("mandatory","true");
                writer.WriteEndElement();
                writer.Flush();
            }

            await Upload(GuncellemeJson,true);

            //var data = new UpdateView()
            //{
            //    version = ProjeVersiyonu,
            //    UploadTarihi = DateTime.Now,
            //    url = GuncellemeDosya,
            //    html = string.Empty,
            //    mandatory = true,
            //};
            //if (!File.Exists(GuncellemeJson))
            //{
            //    File.Create(GuncellemeJson).Dispose();
            //}
            //StreamWriter rd = new StreamWriter(GuncellemeJson, false, Encoding.UTF8);
            //rd.Write(JsonConvert.SerializeObject(data));
            //rd.Close();
        }

        private async Task<string> DosyaIndir(string target)
        {
            try
            {
                var client = new WebClient();
                Uri url = new Uri(target);
                var txt = await client.DownloadStringTaskAsync(url);
                return txt;
            }
            catch (Exception ex)
            {
                return "Hata";
            }
        }

        private List<String> TumDosyalar(String directory)
        {
            return Directory.GetFiles(directory, "*", SearchOption.AllDirectories).ToList();
        }

        private void DosyaSil()
        {
            if (File.Exists(DosyaAdi))
                File.Delete(DosyaAdi);
            if (File.Exists(WebDosyaListesiJson))
                File.Delete(WebDosyaListesiJson);
            if (File.Exists(GuncellemeJson))
                File.Delete(GuncellemeJson);
        }

        private void JsonKaydet(string cfg)
        {
            if (!File.Exists(ProjeAdi + ".json"))
            {
                File.Create(ProjeAdi + ".json").Dispose();
            }
            StreamWriter rd = new StreamWriter(ProjeAdi + ".json");
            rd.Write(cfg);
            rd.Close();
        }

        private async Task Upload(string dosya, bool ps)
        {
            try
            {
                if (ps)
                {
                    await SetMesaj("Dosyalar Yükleniyor");

                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"{FtpAdresi}/{dosya}");
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.Credentials = new NetworkCredential(FtpKullaniciAdi, FtpSifre);
                    Stream ftpStream = request.GetRequestStream();
                    FileStream fs = File.OpenRead(dosya);
                    byte[] buffer = new byte[1024];
                    double total = fs.Length;
                    int byteRead = 0;
                    double read = 0;
                    if (ps)
                        do
                        {
                            byteRead = fs.Read(buffer, 0, 1024);
                            ftpStream.Write(buffer, 0, byteRead);
                            read += byteRead;
                            double percentage = read / total * 100;

                            await SetProggessBar((int)read, (int)total);
                        } while (byteRead != 0);

                    fs.Close();
                    ftpStream.Close();
                }
                else
                {
                    var LoadKontrol = new int[2];
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"{FtpAdresi}/{dosya}");
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.Credentials = new NetworkCredential(FtpKullaniciAdi, FtpSifre);
                    Stream ftpStream = request.GetRequestStream();
                    FileStream fs = File.OpenRead(dosya);
                    byte[] buffer = new byte[1024];
                    double total = fs.Length;
                    LoadKontrol[1] = (int)total;
                    int byteRead = 0;
                    double read = 0;

                    do
                    {
                        byteRead = fs.Read(buffer, 0, 1024);
                        ftpStream.Write(buffer, 0, byteRead);
                        read += byteRead;
                        double percentage = read / total * 100;
                        LoadKontrol[0] = (int)read;

                        //SplashScreenManager.Default.SendCommand(waitFrm.SplashScreenCommand.SetProgress, test);
                    } while (byteRead != 0);
                    fs.Close();
                    ftpStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
