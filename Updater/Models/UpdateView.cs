﻿using System;

namespace Updater.Detmach.Models
{
    public class UpdateView
    {
        public UpdateView()
        {

        }

        public string version { get; set; }

        public string url { get; set; }

        public string changelog { get; set; }

        public string html { get; set; }

        public bool mandatory { get; set; } 

        public DateTime UploadTarihi { get; set; }
    }
}