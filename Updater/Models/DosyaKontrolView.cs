﻿using System;

namespace Updater.Detmach.Models
{
    public class DosyaKontrolView
    {
        public DosyaKontrolView(string dosyaAdi, DateTime degistirilmeTarihi)
        {
            DosyaAdi = dosyaAdi;
            DegistirilmeTarihi = degistirilmeTarihi;
        }

        public string DosyaAdi { get; set; }
        public DateTime DegistirilmeTarihi { get; set; }
    }
}