﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;

namespace Updater.Detmach.Servis
{
    public static class StaticClass
    {
        public static CultureInfo cultureENG = new CultureInfo("en-US", false);

        /// <summary>
        ///  Web İçin Örnek Ahmet Mehmet Cevdet
        ///  ahmet-mehmet-cevdet
        /// </summary>
        /// <param name="str">Düzeltilecek string</param>
        /// <returns></returns>
        public static string kelimeDuzeltEng(this string str)
        {
            str = str.ToLower(cultureENG);
            str = Regex.Replace(str, @"[^A-Za-z0-9 ]", " ");
            str = Regex.Replace(str, @"[^A-Za-z0-9\._]", "-");
            str = Regex.Replace(str, @"---", "-", RegexOptions.RightToLeft);
            str = Regex.Replace(str, @"--", "-", RegexOptions.RightToLeft);
            if (str.Substring(str.Length - 1, 1) == "-")
            {
                str.Substring(0, str.Length - 1);
            }
            else if (str.Substring(0, 1) == "-")
            {
                str.Substring(1, str.Length - 1);
            }
            return str;
        }

        /// <summary>
        /// Serialize an object T using BinaryFormatter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeObjectUsingBinaryFormatter<T>(this T item)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.Serialize(ms, item);
                return UTF8ByteArrayToString(ms.ToArray());
            }
        }

        /// <summary>
        /// Serialize an object T using BinaryFormatter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeObjectUsingBinaryFormatter<T>(this List<T> item)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.Serialize(ms, item);
                return UTF8ByteArrayToString(ms.ToArray());
            }
        }

        /// <summary>
        /// De-serialize a BinaryFormatter-serialized string into an object of type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T DeserializeObjectUsingBinaryFormatter<T>(this string str)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(StringToUTF8ByteArray(str)))
            {
                return (T)serializer.Deserialize(ms);
            }
        }

        /// <summary>
        /// Internal utility method to convert an UTF8 Byte Array to an UTF8 string.
        /// </summary>
        /// <param name="characters"></param>
        /// <returns></returns>
        private static string UTF8ByteArrayToString(byte[] characters)
        {
            return new UTF8Encoding().GetString(characters);
        }

        /// <summary>
        /// Internal utility method to convert an UTF8 string to an UTF8 Byte Array.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static byte[] StringToUTF8ByteArray(string str)
        {
            return new UTF8Encoding().GetBytes(str);
        }
    }
}