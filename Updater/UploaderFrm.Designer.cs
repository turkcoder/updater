﻿namespace Updater.Detmach
{
    partial class UploaderFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploaderFrm));
            this.lblBaslik = new System.Windows.Forms.Label();
            this.lblMesaj = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblBaslik
            // 
            resources.ApplyResources(this.lblBaslik, "lblBaslik");
            this.lblBaslik.Name = "lblBaslik";
            // 
            // lblMesaj
            // 
            resources.ApplyResources(this.lblMesaj, "lblMesaj");
            this.lblMesaj.Name = "lblMesaj";
            // 
            // pb
            // 
            resources.ApplyResources(this.pb, "pb");
            this.pb.Name = "pb";
            this.pb.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.pb, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblMesaj, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblBaslik, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // UploaderFrm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UploaderFrm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UploaderFrm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblBaslik;
        private System.Windows.Forms.Label lblMesaj;
        private System.Windows.Forms.ProgressBar pb;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}