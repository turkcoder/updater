﻿using AutoUpdaterDotNET;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Updater.Detmach.Models;
using Updater.Detmach.Servis;

namespace Updater.Detmach
{
    /// <summary>
    /// 
    /// </summary>
    public class Uploader
    {
        private static readonly string ApplicationYol = Environment.CurrentDirectory;
        private List<DosyaKontrolView> Dosyalar = new List<DosyaKontrolView>();
        private readonly string DosyaAdi;
        private readonly string WebDosyaListesiJson;
        private readonly string GuncellemeJson;
        private readonly string GuncellemeDosya;
        private Mode DownloadMode;
        public enum Modes
        {
            Normal = 0,

            Zorla = 1,
            Zorlaİndir = 2
        }

        /// <summary>
        /// Upload İşlemleri
        /// </summary>
        /// <param name="_ProjeVersiyonu">Projenin Versiyon Numarası</param>
        /// <param name="_ProjeAdi">Proje Adı</param>
        /// <param name="_FtpAdresi"> Projenin Yükleneceği Ftp Adresi ftp://golyok.com//httpdocs/wwwroot/upload/ (Eğer Bir Dizin Belirtecekseniz Lütfen Önce Dizini Ftp'de Oluşturunuz)</param>
        /// <param name="_FtpKullaniciAdi">Projenin Yükleneceği Ftp Kullanıcı Adı</param>
        /// <param name="_FtpSifre">Projenin Yükleneceği Ftp Kullanıcı Şifre</param>
        /// <param name="_ProjeDownloadUrl">Download Adresi (Sonuna Slash Katmayı Unutma - http://www.golyok.com/update/</param>
        public Uploader(string _ProjeVersiyonu, string _ProjeAdi, string _FtpAdresi, string _FtpKullaniciAdi, string _FtpSifre, string _ProjeDownloadUrl)
        {
            ProjeVersiyonu = _ProjeVersiyonu;
            ProjeAdi = _ProjeAdi;
            FtpAdresi = _FtpAdresi;
            FtpKullaniciAdi = _FtpKullaniciAdi;
            FtpSifre = _FtpSifre;
            ProjeUrl = _ProjeDownloadUrl;
            DosyaAdi = ProjeAdi.kelimeDuzeltEng() + ".zip";
            WebDosyaListesiJson = ProjeAdi.kelimeDuzeltEng() + ".json";
            GuncellemeJson = ProjeAdi.kelimeDuzeltEng() + "-ver.xml";
            GuncellemeDosya = ProjeUrl + DosyaAdi;

        }
        /// <summary>
        /// Güncelleme Varmı kontro
        /// </summary>
        public void GuncellemeKontrol(bool ErteleButon = true, bool DahaSonraHatirlat = true, Modes mode = Modes.Normal)
        {
            AutoUpdater.UpdateMode = (Mode)Enum.ToObject(typeof(Mode), (int)mode);
            AutoUpdater.Start(ProjeUrl + GuncellemeJson);
            AutoUpdater.ShowSkipButton = ErteleButon;
            AutoUpdater.ShowRemindLaterButton = DahaSonraHatirlat;
            //AutoUpdater.ParseUpdateInfoEvent += AutoUpdaterOnParseUpdateInfoEvent;

        }


        private void AutoUpdaterOnParseUpdateInfoEvent(ParseUpdateInfoEventArgs args)
        {
            dynamic json = JsonConvert.DeserializeObject(args.RemoteData);
            args.UpdateInfo = new UpdateInfoEventArgs
            {
                CurrentVersion = json.version,
                ChangelogURL = json.changelog,
                Mandatory = json.mandatory,
                DownloadURL = json.url
            };
        }
        /// <summary>
        /// Proje Adı
        /// </summary>
        private string ProjeAdi { get; set; }

        /// <summary>
        /// Projenin Versiyon Numarası
        /// </summary>
        private string ProjeVersiyonu { get; set; }

        /// <summary>
        /// Download Adresi (Sonuna Slash Katmayı Unutma - http://www.golyok.com/update/
        /// </summary>
        private string ProjeUrl { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Adresi ftp://golyok.com//httpdocs/wwwroot/upload/ (Eğer Bir Dizin Belirtecekseniz Lütfen Önce Dizini Ftp'de Oluşturunuz)
        /// </summary>
        private string FtpAdresi { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Kullanıcı Adı
        /// </summary>
        private string FtpKullaniciAdi { get; set; }

        /// <summary>
        /// Projenin Yükleneceği Ftp Kullanıcı Şifresi
        /// </summary>
        private string FtpSifre { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task GuncellemeYukle()
        {
            try
            {
                UploaderFrm frm = new UploaderFrm(ProjeVersiyonu, ProjeAdi, FtpAdresi, FtpKullaniciAdi, FtpSifre, ProjeUrl);
                frm.ShowDialog();
                //DosyaSil();
                //#region Dosya Listeleri

                //string WebFileList = await DosyaIndir(ProjeUrl + WebDosyaListesiJson);
                //List<DosyaKontrolView> WebDosyaListesi;
                //if (WebFileList == "Hata")
                //{
                //    WebDosyaListesi = new List<DosyaKontrolView>();
                //}
                //else
                //{
                //    WebDosyaListesi = JsonConvert.DeserializeObject<List<DosyaKontrolView>>(WebFileList);
                //}
                //List<string> DosyaListesi = TumDosyalar(ApplicationYol);
                //foreach (var item in DosyaListesi)
                //{
                //    var DosyaZaman = File.GetLastWriteTime(item);
                //    var dosya = WebDosyaListesi.Where(t => t.DosyaAdi == item.Substring(folderOffset)).FirstOrDefault();
                //    if (dosya != null)
                //    {
                //        if (dosya.DegistirilmeTarihi != DosyaZaman)
                //        {
                //            WebDosyaListesi.Remove(dosya);
                //            Dosyalar.Add(new DosyaKontrolView(item, DosyaZaman));
                //        }
                //    }
                //    else { Dosyalar.Add(new DosyaKontrolView(item, DosyaZaman)); }
                //}

                //#endregion Dosya Listeleri

                //#region Zipi Oluştur


                //FileStream fsOut = File.Create(DosyaAdi);
                //ZipOutputStream zipStream = new ZipOutputStream(fsOut);
                //zipStream.SetLevel(9); //0-9, 9 being the highest level of compression

                //UploaderFrm frm = new UploaderFrm();
                //frm.Show();
                //frm.SetMesaj("Dosyalar Sıkıştırılıyor");
                //int s = 0;

                //foreach (var t in Dosyalar)
                //{
                //    s++;
                //    string entryName = t.DosyaAdi.Substring(folderOffset);
                //    WebDosyaListesi.Add(new DosyaKontrolView(entryName, t.DegistirilmeTarihi));
                //    FileInfo fi = new FileInfo(t.DosyaAdi);
                //    entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                //    ZipEntry newEntry = new ZipEntry(entryName);
                //    newEntry.Flags = (int)ICSharpCode.SharpZipLib.Zip.GeneralBitFlags.UnicodeText;
                //    newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity
                //    newEntry.Size = fi.Length;
                //    zipStream.PutNextEntry(newEntry);

                //    // Zip the file in buffered chunks
                //    // the "using" will close the stream even if an exception occurs
                //    byte[] buffer = new byte[4096];
                //    using (FileStream streamReader = File.OpenRead(t.DosyaAdi))
                //    {
                //        StreamUtils.Copy(streamReader, zipStream, buffer);
                //    }
                //    zipStream.CloseEntry();
                //    await frm.SetProggessBar(s, Dosyalar.Count());
                //}
                //zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
                //zipStream.Close();
                //frm.Dispose();
                //#endregion Zipi Oluştur

                //#region Dosya Listelerini Webe Yükle
                //StreamWriter rd = new StreamWriter(WebDosyaListesiJson,false, Encoding.UTF8);

                //rd.Write(JsonConvert.SerializeObject(WebDosyaListesi));
                //rd.Close();
                //Upload(WebDosyaListesiJson, false);

                //#endregion Dosya Listelerini Webe Yükle
                //GuncellemeDosyasiHazirla();
                //Upload(DosyaAdi, true);
                //DosyaSil();
                //Task.CompletedTask;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}