﻿using System;
using System.Windows.Forms;
using Updater.Detmach;

namespace UploaderTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static string ProjeAdi = "Deneme A.q Projesi";
        static string ProjeVersiyonu = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        static string FtpAdresi = "ftp://golyok.com//httpdocs/wwwroot/upload/";
        static string FtpKullaniciAdi = "golyok";
        static string FtpSifre = "99721952";
        static string ProjeUrl = "https://www.golyok.com/upload/";
        Uploader uploader = new Uploader(ProjeVersiyonu, ProjeAdi, FtpAdresi, FtpKullaniciAdi, FtpSifre, ProjeUrl);
        private void Form1_Load(object sender, EventArgs e)
        {
            uploader.GuncellemeKontrol(mode:Uploader.Modes.Zorlaİndir);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            await uploader.GuncellemeYukle();
        }
    }
}