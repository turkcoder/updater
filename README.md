﻿# Kullanım

### Dosyaları Güncellemek İçin

```csharp
var ProjeAdi = "Deneme Projesi";
var ProjeVersiyonu = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
var FtpAdresi = "ftp://golyok.com//httpdocs/wwwroot/upload/";
var FtpKullaniciAdi = "golyok";
var FtpSifre = "password";
var ProjeUrl = "https://www.golyok.com/upload/";
Uploader uploader = new Uploader(ProjeVersiyonu, ProjeAdi, FtpAdresi, FtpKullaniciAdi, FtpSifre, ProjeUrl)
await uploader.GuncellemeYukle();
```

### Program Güncelleme Sorgulama ve Güncelleme Yapmak İçin

```csharp
uploader.GuncellemeKontrol();
```